const Bull = use('Rocketseat/Bull')
const Env = use('Env')
const StudentCollectibles = use('App/Jobs/Collectibles/StudentCollectibles')

Bull.process().ui(Env.get('BULL_UI_PORT') || 9999)
