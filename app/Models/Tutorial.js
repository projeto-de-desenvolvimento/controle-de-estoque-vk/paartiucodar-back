/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Tutorial extends Model {
	static boot() {
		super.boot()

		this.addTrait('@provider:Lucid/Slugify', {
			fields: {
				sys_id: 'title',
			},
			strategy: 'dbIncrement',
		})
	}

	class() {
		return this.belongsTo('App/Models/Class')
	}

	tutorialFeedbacks() {
		return this.hasMany('App/Models/TutorialFeedback')
	}

	studentTutorial() {
		return this.hasMany('App/Models/StudentTutorial')
	}
}

module.exports = Tutorial
