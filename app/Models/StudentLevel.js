"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class StudentLevel extends Model {
  level() {
    this.belongsTo("App/Model/Level");
  }
  student() {
    this.belongsTo("App/Model/Student");
  }
}

module.exports = StudentLevel;
