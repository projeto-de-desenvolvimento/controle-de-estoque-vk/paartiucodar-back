'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Collectible extends Model {
	studentCollectibles() {
		return this.hasMany('App/Models/StudentCollectible')
	}
}

module.exports = Collectible
