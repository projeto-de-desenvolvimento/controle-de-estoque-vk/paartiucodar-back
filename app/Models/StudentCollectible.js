'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class StudentCollectible extends Model {
	student() {
		return this.belongsTo('App/Models/Student')
	}
	collectible() {
		return this.belongsTo('App/Models/Collectible')
	}
}

module.exports = StudentCollectible
