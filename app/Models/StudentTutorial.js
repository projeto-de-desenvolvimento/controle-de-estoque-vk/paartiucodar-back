'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class StudentTutorial extends Model {
	student() {
		return this.belongsTo('App/Models/Student')
	}

	tutorial() {
		return this.belongsTo('App/Models/Tutorial')
	}
}

module.exports = StudentTutorial
