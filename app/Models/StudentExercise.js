'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class StudentExercise extends Model {
	student() {
		return this.belongsTo('App/Models/Student')
	}
	exercise() {
		return this.belongsTo('App/Models/Exercise')
	}
}

module.exports = StudentExercise
