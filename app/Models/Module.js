/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Module extends Model {
	static boot() {
		super.boot()

		this.addTrait('@provider:Lucid/Slugify', {
			fields: {
				sys_id: 'name',
			},
			strategy: 'dbIncrement',
			// strategy: async (field, value, modelInstance) =>
			// 	`modulo-${String(modelInstance.name).padStart(2, '0')}`,
		})
	}

	students() {
		return this.belongsToMany('App/Models/Student')
	}

	classes() {
		return this.hasMany('App/Models/Class')
	}
}

module.exports = Module
