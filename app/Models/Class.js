"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class Class extends Model {
  static boot() {
    super.boot();

    this.addTrait("@provider:Lucid/Slugify", {
      fields: {
        sys_id: "title",
      },
      strategy: async (field, value, modelInstance) =>
        `tutorial-${String(modelInstance.order).padStart(2, "0")}`,
    });
  }
  module() {
    return this.belongsTo("App/Models/Module");
  }
  exercises() {
    return this.hasMany("App/Models/Exercise")
  }
  tutorials() {
    return this.hasMany("App/Models/Tutorial");
  }
}

module.exports = Class;
