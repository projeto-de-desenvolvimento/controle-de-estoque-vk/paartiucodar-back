"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class TutorialFeedback extends Model {
  tutorial() {
    return this.belongsTo("App/Models/Tutorial");
  }
  student() {
    return this.belongsTo("App/Models/Student");
  }
}

module.exports = TutorialFeedback;
