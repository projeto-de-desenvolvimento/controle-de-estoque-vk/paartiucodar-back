"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class Level extends Model {
  studentLevel() {
    return this.hasMany("App/Models/StudentLevel");
  }
}

module.exports = Level;
