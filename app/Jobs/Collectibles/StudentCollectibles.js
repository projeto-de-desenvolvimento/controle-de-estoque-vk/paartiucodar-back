const Mail = use('Mail')
const Student = use('App/Models/Student')
const StudentCollectible = use('App/Models/StudentCollectible')
const moment = use('moment')

class StudentCollectibles {
	static get key() {
		return 'Student|StudentCollectibles'
	}

	async handle(job) {
		const { data } = job // the 'data' variable has user data

		const student = await Student.query()
			.with('studentTutorial')
			.with('studentExercise')
			.with('studentResponse')
			.where('id', data.id)
			.first()

		const total_student_tutorial = student.toJSON().studentTutorial.length
		const total_student_exercise = student.toJSON().studentExercise.length
		const total_student_questions = student.toJSON().studentResponse.length
		let student_collectible
		let student_exercise

		switch (total_student_tutorial) {
			case 1:
				student_collectible = await StudentCollectible.query()
					.where('student_id', data.id)
					.where('collectible_id', 1)
					.first()

				if (student_collectible) {
					await StudentCollectible.create({
						student_id: data.id,
						collectible_id: 1,
						acquired_on: moment().format('YYYY-MM-DD HH:mm:ss'),
					})
				}
				break
			case 10:
				student_collectible = await StudentCollectible.query()
					.where('student_id', data.id)
					.where('collectible_id', 3)
					.first()
				if (student_collectible) {
					await StudentCollectible.create({
						student_id: data.id,
						collectible_id: 3,
						acquired_on: moment().format('YYYY-MM-DD HH:mm:ss'),
					})
				}
				break
			default:
				break
		}
		switch (total_student_exercise) {
			case 1:
				student_collectible = await StudentCollectible.query()
					.where('student_id', data.id)
					.where('collectible_id', 2)
					.first()
				if (student_collectible) {
					await StudentCollectible.create({
						student_id: data.id,
						collectible_id: 2,
						acquired_on: moment().format('YYYY-MM-DD HH:mm:ss'),
					})
				}
				break
			case 10:
				student_collectible = await StudentCollectible.query()
					.where('student_id', data.id)
					.where('collectible_id', 4)
					.first()
				if (student_collectible) {
					await StudentCollectible.create({
						student_id: data.id,
						collectible_id: 4,
						acquired_on: moment().format('YYYY-MM-DD HH:mm:ss'),
					})
				}
				break
			default:
				break
		}
		switch (total_student_questions) {
			case 1:
				student_collectible = await StudentCollectible.query()
					.where('student_id', data.id)
					.where('collectible_id', 5)
					.first()
				if (student_collectible) {
					await StudentCollectible.create({
						student_id: data.id,
						collectible_id: 5,
						acquired_on: moment().format('YYYY-MM-DD HH:mm:ss'),
					})
				}
				break
			case 10:
				student_collectible = await StudentCollectible.query()
					.where('student_id', data.id)
					.where('collectible_id', 6)
					.first()
				if (student_collectible) {
					await StudentCollectible.create({
						student_id: data.id,
						collectible_id: 6,
						acquired_on: moment().format('YYYY-MM-DD HH:mm:ss'),
					})
				}
				break
			default:
				break
		}
	}
}

module.exports = StudentCollectibles
