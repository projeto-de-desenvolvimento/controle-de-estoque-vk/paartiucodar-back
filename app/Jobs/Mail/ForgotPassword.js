const Mail = use('Mail')

class ForgotPassword {
	static get key() {
		return 'Mail|ForgotPassword'
	}

	async handle(job) {
		const { data } = job // the 'data' variable has user data

		await Mail.send('mails.password_reset', data.mailData, (message) => {
			message.to(data.email).from('conig@gmail.com').subject('Resetar a senha')
		})

		return data
	}
}

module.exports = ForgotPassword
