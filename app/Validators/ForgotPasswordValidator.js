const Antl = use('Antl')

class ForgotPasswordValidator {
	get validateAll() {
		return true
	}

	get rules() {
		return {
			email: 'exists:students,email|email|required',
		}
	}

	get sanitizationRules() {
		return {
			email: 'trim',
		}
	}

	get messages() {
		return Antl.forLocale('pt').list('validation')
	}
}

module.exports = ForgotPasswordValidator
