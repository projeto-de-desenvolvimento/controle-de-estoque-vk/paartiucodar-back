/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Student = use('App/Models/Student')
const { validate } = use('indicative')
/**
 * Resourceful controller for interacting with students
 */

class StudentController {
	/**
	 * Show a list of all students.
	 * GET students
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async index({ request, response, view }) {
		const students = await Student.all()

		return students
	}

	/**
	 * Create/save a new student.
	 * POST students
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async store({ request, response }) {
		const { complete_name, username, email, password } = request.only([
			'complete_name',
			'username',
			'email',
			'password',
		])

		const student = await Student.create({
			complete_name,
			username: username.replace(/[^a-zA-Zs]/g, '').trim(),
			email,
			password,
		})

		return student
	}

	/**
	 * Display a single student.
	 * GET students/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async show({ params, request, response, view }) {
		const student = await Student.findOrFail(params.id)

		return response.status(200).json(student.toJSON())
	}

	/**
	 * Update student details.
	 * PUT or PATCH students/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async update({ params, request, response }) {
		const student = await Student.findOrFail(params.id)

		const data = request.only(['complete_name', 'username', 'email', 'password'])

		student.merge(data)
		await student.save()

		return student
	}

	/**
	 * Delete a student with id.
	 * DELETE students/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async destroy({ params, request, response }) {
		const student = await Student.findOrFail(params.id)

		await student.delete()
	}
}

module.exports = StudentController
