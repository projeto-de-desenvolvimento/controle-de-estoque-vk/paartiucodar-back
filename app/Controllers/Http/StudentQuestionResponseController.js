const StudentResponseQuestion = use('App/Models/StudentResponseQuestion')
const Student = use('App/Models/Student')
const StudentXp = use('App/Models/StudentXp')
const Question = use('App/Models/Question')
const Bull = use('Rocketseat/Bull')
const StudentCollectibles = use('App/Jobs/Collectibles/StudentCollectibles')

class StudentQuestionResponseController {
	async store({ params, request, response }) {
		let response_ = false
		const { question_id, questionResponse, student_id, studentResponse } = request.only([
			'question_id',
			'questionResponse',
			'student_id',
			'studentResponse',
		])

		const student = await Student.query().where('id', student_id).first()
		const question = await Question.query().where('id', question_id).first()
		let experience_gained = 0

		if (question.response.toLowerCase() === studentResponse.toLowerCase()) {
			switch (question.difficulty) {
				case 1:
					experience_gained += 50
					break
				case 2:
					experience_gained += 75
					break
				case 3:
					experience_gained += 100
					break
				default:
					experience_gained += 50
					break
			}
			student.experience += experience_gained
			response_ = true
		} else {
			switch (question.difficulty) {
				case 1:
					experience_gained += 25
					break
				case 2:
					experience_gained += 30
					break
				case 3:
					experience_gained += 35
					break
				default:
					experience_gained += 25
					break
			}
			student.experience += experience_gained
			response_ = false
		}
		student.save()

		await StudentXp.create({
			student_id: student.id,
			experience: experience_gained,
		})
		const responseQuestion = await StudentResponseQuestion.create({
			question_id,
			student_id,
			response: studentResponse,
		})

		Bull.add(StudentCollectibles.key, {
			id: student.id,
			levelup: false,
		})

		return response.status(200).json(response_)
	}
}

module.exports = StudentQuestionResponseController
