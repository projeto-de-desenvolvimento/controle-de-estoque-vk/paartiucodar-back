const Student = use('App/Models/Student')
const Helpers = use('Helpers')
const StudentCollectibles = use('App/Jobs/Collectibles/StudentCollectibles')
const Bull = use('Rocketseat/Bull')

class ProfileController {
	async index({ params, response, auth }) {
		const { id } = await auth.getUser()
		const student = await Student.query()
			.select(
				'id',
				'complete_name',
				'email',
				'level',
				'profile_pic',
				'biography',
				'username',
				'experience'
			)
			.with('studentTutorial', (builder) =>
				builder
					.select('id', 'tutorial_id', 'student_id')
					.with('tutorial', (tutorialBuilder) => tutorialBuilder.select('id', 'title'))
			)
			.with('studentExercise', (builder) =>
				builder
					.select('id', 'exercise_id', 'student_id')
					.with('exercise', (tutorialBuilder) => tutorialBuilder.select('id', 'title'))
			)
			.with('studentCollectibles', (builder) =>
				builder
					.select('id', 'student_id', 'collectible_id')
					.with('collectible', (collectibleBuilder) =>
						collectibleBuilder.select('id', 'description', 'svg')
					)
			)
			.where('id', id)
			.first()
		const level = Math.floor(Math.sqrt(student.experience / 100))

		const progress =
			((student.experience - level * level * 100) * 100) / ((level + 1) * (level + 1) * 100)

		if (level > student.level) {
			student.level = level
			await student.save()
			student.level_up = true
			Bull.add(
				StudentCollectibles.key,
				{
					id,
					levelup: student.level_up,
				},
				{}
			)
		} else {
			student.level_up = false
		}
		if (!student.level) {
			student.level = 0
		}
		student.progress = progress.toFixed(2)

		student.password = ''

		return response.status(200).json(student)
	}

	async show({ params, request, response }) {
		const { id } = params
		const student = await Student.query()
			.with('studentTutorial', (builder) =>
				builder
					.select('id', 'tutorial_id', 'student_id')
					.with('tutorial', (tutorialBuilder) => tutorialBuilder.select('id', 'title'))
			)
			.with('studentExercise', (builder) =>
				builder
					.select('id', 'exercise_id', 'student_id')
					.with('exercise', (tutorialBuilder) => tutorialBuilder.select('id', 'title'))
			)
			.with('studentCollectibles', (builder) =>
				builder
					.select('id', 'student_id', 'collectible_id')
					.with('collectible', (collectibleBuilder) =>
						collectibleBuilder.select('id', 'description', 'svg')
					)
			)
			.where('username', id)
			.first()

		return student
	}

	async update({ params, request, response, auth }) {
		const { id } = params
		const { completeName, informations, biography } = request.only([
			'completeName',
			'informations',
			'biography',
		])

		const student = await Student.query().where('id', id).first()

		const validationOptions = {
			types: ['image'],
			size: '2mb',
			extnames: ['png', 'jpg', 'jpeg'],
		}

		if (!informations) {
			const profilepic = request.file('avatar', validationOptions)
			let profilepicName

			if (profilepic != null) {
				if (student.profile_picture != null) {
					profilepicName = student.profile_picture
				} else {
					profilepicName = `${new Date().getTime()}.${profilepic.subtype}`
					student.profile_pic = profilepicName
				}
				await profilepic.move(Helpers.publicPath('uploads/profile_pic'), {
					name: profilepicName,
					overwrite: true,
				})
			}

			student.profile_pic = profilepicName || null
		}
		student.complete_name = completeName
		student.biography = biography
		student.save()

		return response.status(200).json(student.toJSON())
	}
}

module.exports = ProfileController
