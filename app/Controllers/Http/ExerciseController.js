/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Exercise = use('App/Models/Exercise')
const StudentExercise = use('App/Models/StudentExercise')
const Tutorial = use('App/Models/Tutorial')
/**
 * Resourceful controller for interacting with exercises
 */

class ExerciseController {
	/**
	 * Show a list of all exercises.
	 * GET exercises
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async index({ request, response, view }) {}

	/**
	 * Render a form to be used for creating a new exercise.
	 * GET exercises/create
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async create({ request, response, view }) {}

	/**
	 * Create/save a new exercise.
	 * POST exercises
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async store({ request, response }) {}

	/**
	 * Display a single exercise.
	 * GET exercises/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async show({ params, request, response, view, auth }) {
		const user = await auth.getUser()

		const student_exercises = await StudentExercise.query()
			.where('student_id', user.id)
			.where('exercise_id', params.id)
			.first()

		const exercise = await Exercise.query()
			.with('questions', (builder) =>
				builder
					.orderBy('position', 'asc')
					.with('studentResponse', (responseBuilder) =>
						responseBuilder.where('student_id', user.id)
					)
			)
			.where('id', params.id)
			.first()

		const next_exercise = await Exercise.query()
			.with('class', (builder) => builder.select('id', 'module_id'))
			.where('class_id', exercise.toJSON().class_id)
			.where('position', exercise.toJSON().position + 1)
			.first()

		exercise.is_completed = !!student_exercises
		if (!next_exercise) {
			exercise.next_exercise = next_exercise
				? `/module/${next_exercise.toJSON().class.module_id}/tutorial/${next_exercise.toJSON().id}`
				: 'last'
		} else {
			exercise.next_exercise = `/module/${next_exercise.toJSON().class.module_id}/exercise/${
				next_exercise.toJSON().id
			}`
		}

		return response.status(200).json(exercise.toJSON())
	}

	/**
	 * Render a form to update an existing exercise.
	 * GET exercises/:id/edit
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async edit({ params, request, response, view }) {}

	/**
	 * Update exercise details.
	 * PUT or PATCH exercises/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async update({ params, request, response }) {}

	/**
	 * Delete a exercise with id.
	 * DELETE exercises/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async destroy({ params, request, response }) {}
}

module.exports = ExerciseController
