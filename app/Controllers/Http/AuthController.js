const Student = use('App/Models/Student')

class AuthController {
	async store({ request, response, auth }) {
		const { email, password } = request.all()

		try {
			if (await auth.authenticator('jwt').attempt(email, password)) {
				const student = await Student.findBy('email', email)

				const token = await auth.authenticator('jwt').generate(student)

				Object.assign(student, token)

				return response.json(student)
			}
		} catch (e) {
			console.log(e)

			return response.status(401).json({
				message: {
					pt: 'Você não está registrado!',
					en: 'You are not registered!',
				},
			})
		}

		return ''
	}
}

module.exports = AuthController
