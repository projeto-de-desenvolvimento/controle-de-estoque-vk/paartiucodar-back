const Student = use('App/Models/Student')
const StudentXp = use('App/Models/StudentXp')
const StudentTutorial = use('App/Models/StudentTutorial')
const StudentCollectibles = use('App/Jobs/Collectibles/StudentCollectibles')
const Bull = use('Rocketseat/Bull')

class StudentTutorialController {
	async store({ request, auth, response }) {
		const { tutorial_id } = request.only(['tutorial_id'])
		const user = await auth.getUser()
		const student = await Student.query().where('id', user.id).first()

		const studentTutorial = await StudentTutorial.create({
			student_id: user.id,
			tutorial_id,
		})

		await StudentXp.create({
			student_id: student.id,
			experience: 50,
		})
		student.experience += 50
		student.save()

		Bull.add(StudentCollectibles.key, {
			id: student.id,
			levelup: false,
		})

		return response.status(200).json(studentTutorial.toJSON())
	}
}

module.exports = StudentTutorialController
