const Student = use('App/Models/Student')
const Bull = use('Rocketseat/Bull')
const ForgotPassword = use('App/Jobs/Mail/ForgotPassword')
const randomString = require('random-string')

class ForgotPasswordController {
	async store({ request, response }) {
		const { email } = request.only(['email'])

		const student = await Student.findBy('email', email)

		student.confirmation_token = randomString({ length: 40 })
		await student.save()

		const mailData = {
			confirmation_token: student.confirmation_token,
		}

		Bull.add(
			ForgotPassword.key,
			{
				mailData,
				email: student.email,
			},
			{
				attempts: 5,
				backoff: 1000,
			}
		)

		return response.status(200).json({ token: student.confirmation_token })
	}
}

module.exports = ForgotPasswordController
