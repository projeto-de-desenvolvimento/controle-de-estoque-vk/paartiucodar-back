const { hooks } = use('@adonisjs/ignitor')

hooks.after.providersBooted(() => {
	const Exception = use('Exception')
	const Validator = use('Validator')
	const Database = use('Database')

	Exception.handle('InvalidSessionException', (error, { response }) => response.redirect('/login'))

	const uniqueUsername = async (data, field, message, args, get) => {
		console.log(data, field, message, args, get)
		const value = get(data, field)
		const [table, column] = args

		const row = await Database.table(table).where(field, value).first()

		if (row && `${row.id}` !== `${get(data, column)}`) {
			throw message
		}
	}
	const exists = async (data, field, message, args, get) => {
		const value = get(data, field)

		if (!value) {
			return
		}

		const [table, column] = args
		const row = await Database.table(table).where(column, value).first()

		if (!row) {
			throw message
		}
	}
	Validator.extend('exists', exists)
	Validator.extend('uniqueUsername', uniqueUsername)
})
