const Env = use('Env')

module.exports = {
	connection: Env.get('BULL_CONNECTION', 'bull'),
	bull: {
		redis: {
			host: Env.get('REDIS_HOST'),
			port: Env.get('REDIS_PORT'),
			password: Env.get('REDIS_PASSWORD'),
			db: Env.get('REDIS_DB'),
			keyPrefix: Env.get('REDIS_KEY_PREFIX'),
		},
	},
	remote: Env.get('REDIS_REMOTE'),
}
