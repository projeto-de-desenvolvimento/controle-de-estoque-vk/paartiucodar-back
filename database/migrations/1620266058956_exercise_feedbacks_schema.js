"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class ExerciseFeedbacksSchema extends Schema {
  up() {
    this.create("exercise_feedbacks", (table) => {
      table.increments();
      table.enu("feedback_type", ["GOSTEI", "ERRO", "OUTROS"]).notNullable();
      table.string("feedback_text").notNullable();
      table
        .integer("exercise_id")
        .unsigned()
        .references("id")
        .inTable("exercises")
        .onDelete("CASCADE")
        .onUpdate("CASCADE")
        .notNullable();
      table
        .integer("student_id")
        .unsigned()
        .references("id")
        .inTable("students")
        .onDelete("CASCADE")
        .onUpdate("CASCADE")
        .notNullable();
      table.timestamps();
    });
  }

  down() {
    this.drop("exercise_feedbacks");
  }
}

module.exports = ExerciseFeedbacksSchema;
