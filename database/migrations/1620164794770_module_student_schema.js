"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class ModuleStudentSchema extends Schema {
  up() {
    this.create("module_student", (table) => {
      table
        .integer("student_id")
        .unsigned()
        .references("id")
        .inTable("students")
        .onDelete("CASCADE")
        .onUpdate("CASCADE")
        .notNullable();
      table
        .integer("module_id")
        .unsigned()
        .references("id")
        .inTable("modules")
        .onDelete("CASCADE")
        .onUpdate("CASCADE")
        .notNullable();
      table.boolean("completed").defaultTo(false);
      table.date("completed_on");
      table.integer("times_completed").defaultTo(0);
      table.boolean("blocked").defaultTo(false);
    });
  }

  down() {
    this.drop("module_student");
  }
}

module.exports = ModuleStudentSchema;
