'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StudentExerciseSchema extends Schema {
	up() {
		this.create('student_exercises', (table) => {
			table.increments()
			table
				.integer('exercise_id')
				.unsigned()
				.references('id')
				.inTable('exercises')
				.onDelete('CASCADE')
				.onUpdate('CASCADE')
				.notNullable()
			table
				.integer('student_id')
				.unsigned()
				.references('id')
				.inTable('students')
				.onDelete('CASCADE')
				.onUpdate('CASCADE')
				.notNullable()
			table.timestamps()
		})
	}

	down() {
		this.drop('student_exercises')
	}
}

module.exports = StudentExerciseSchema
