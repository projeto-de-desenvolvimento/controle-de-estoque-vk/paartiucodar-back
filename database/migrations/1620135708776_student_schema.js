'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StudentSchema extends Schema {
	up() {
		this.create('students', (table) => {
			table.increments()
			table.string('complete_name', 30).notNullable()
			table.string('username', 15).notNullable().unique()
			table.text('biography').nullable()
			table.string('email').notNullable()
			table.string('password', 60).notNullable()
			table.integer('experience').nullable()
			table.string('profile_pic').nullable()
			table.string('profile_picture').nullable()
			table.timestamps()
		})
	}

	down() {
		this.drop('students')
	}
}

module.exports = StudentSchema
