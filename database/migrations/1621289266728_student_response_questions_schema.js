'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StudentResponseQuestionsSchema extends Schema {
	up() {
		this.create('student_response_questions', (table) => {
			table.increments()
			table
				.integer('student_id')
				.unsigned()
				.notNullable()
				.references('id')
				.inTable('students')
				.onDelete('CASCADE')
				.onUpdate('CASCADE')
				.notNullable()
			table
				.integer('question_id')
				.unsigned()
				.notNullable()
				.references('id')
				.inTable('questions')
				.onDelete('CASCADE')
				.onUpdate('CASCADE')
				.notNullable()
			table.boolean('response')
			table.timestamps()
		})
	}

	down() {
		this.drop('student_response_questions')
	}
}

module.exports = StudentResponseQuestionsSchema
