/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChangeQuestionsSchema extends Schema {
	up() {
		this.table('questions', (table) => {
			table.integer('difficulty').after('response').default(1).notNullable().unsigned()
		})
	}

	down() {
		this.table('questions', (table) => {
			table.dropColumn('difficulty')
		})
	}
}

module.exports = ChangeQuestionsSchema
