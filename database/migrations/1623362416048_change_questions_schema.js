'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChangeQuestionsSchema extends Schema {
	up() {
		this.table('questions', (table) => {
			table.enum('type', ['VF', 'MULTIPLE', 'STRING']).notNullable().default('VF').after('question')
			table.string('alternative_1').nullable().after('question')
			table.string('alternative_2').nullable().after('alternative_1')
			table.string('alternative_3').nullable().after('alternative_2')
			table.string('alternative_4').nullable().after('alternative_3')
		})
	}

	down() {
		this.table('questions', (table) => {
			table.dropColumn('alternative_1')
			table.dropColumn('alternative_2')
			table.dropColumn('alternative_3')
			table.dropColumn('alternative_4')
		})
	}
}

module.exports = ChangeQuestionsSchema
