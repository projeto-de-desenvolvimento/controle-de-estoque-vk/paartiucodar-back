/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChangeStudentsSchema extends Schema {
	up() {
		this.table('students', (table) => {
			table.string('confirmation_token').after('profile_picture')
			table.int('level').after('confirmation_token')
		})
	}

	down() {
		this.table('students', (table) => {
			table.dropColumn('confirmation_token')
			table.dropColumn('level')
		})
	}
}

module.exports = ChangeStudentsSchema
