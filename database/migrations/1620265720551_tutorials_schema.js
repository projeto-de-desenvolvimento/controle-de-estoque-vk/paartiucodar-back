'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TutorialsSchema extends Schema {
	up() {
		this.create('tutorials', (table) => {
			table.increments()
			//Declaração de variáveis
			table.string('title').notNullable()
			//For future use
			table.string('sys_id').notNullable()
			//posição na classe
			table.integer('position').notNullable()
			//Entendendo var, let e const
			table.string('description').notNullable()
			//texto do tutorial
			table.text('content').notNullable()
			table
				.integer('class_id')
				.unsigned()
				.references('id')
				.inTable('classes')
				.onDelete('CASCADE')
				.onUpdate('CASCADE')
				.notNullable()
			table.timestamps()
		})
	}

	down() {
		this.drop('tutorials')
	}
}

module.exports = TutorialsSchema
