'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChangeExercisesSchema extends Schema {
	up() {
		this.table('exercises', (table) => {
			table.dropColumn('difficulty')
		})
	}

	down() {
		this.table('exercises', (table) => {
			table.enu('difficulty', ['EASY', 'MEDIUM', 'HARD']).notNullable()
		})
	}
}

module.exports = ChangeExercisesSchema
