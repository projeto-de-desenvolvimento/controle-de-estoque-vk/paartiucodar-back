'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StudentProfileSchema extends Schema {
	up() {
		this.create('student_profiles', (table) => {
			table.increments()
			table.string('picture').nullable()
			//biografia da pessoa
			table.string('description').nullable()
			table.dateTime('member_since').notNullable()
			table.string('hobbies').nullable()
			table.string('favorite_prog_language').nullable()
			table
				.integer('student_id')
				.unsigned()
				.references('id')
				.inTable('students')
				.onDelete('CASCADE')
				.onUpdate('CASCADE')
				.notNullable()
			table.timestamps()
		})
	}

	down() {
		this.drop('student_profiles')
	}
}

module.exports = StudentProfileSchema
