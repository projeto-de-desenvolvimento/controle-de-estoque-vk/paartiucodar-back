"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class CollegesSchema extends Schema {
  up() {
    this.create("colleges", (table) => {
      table.increments();
      table.string("mec_name").notNullable();
      table.string("location").notNullable();
      table.enu("type", ["SENAC, INSTITUTO_FEDERAL, OUTROS"]);
      table.timestamps();
    });
  }

  down() {
    this.drop("colleges");
  }
}

module.exports = CollegesSchema;
