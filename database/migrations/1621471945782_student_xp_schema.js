'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StudentXpSchema extends Schema {
	up() {
		this.create('student_xps', (table) => {
			table.increments()
			table
				.integer('student_id')
				.unsigned()
				.references('id')
				.inTable('students')
				.onDelete('CASCADE')
				.onUpdate('CASCADE')
				.notNullable()
			table.integer('experience').notNullable()
			table.timestamps()
		})
	}

	down() {
		this.drop('student_xps')
	}
}

module.exports = StudentXpSchema
