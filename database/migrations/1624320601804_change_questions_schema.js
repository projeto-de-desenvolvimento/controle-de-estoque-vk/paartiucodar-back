'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChangeQuestionsSchema extends Schema {
	up() {
		this.table('questions', (table) => {
			table.string('response').notNullable().alter()
		})
	}

	down() {
		this.table('questions', (table) => {
			table.boolean('response')
		})
	}
}

module.exports = ChangeQuestionsSchema
