'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ExercisesSchema extends Schema {
	up() {
		this.create('exercises', (table) => {
			table.increments()
			//For future use
			table.string('sys_id').notNullable()
			//Exercícios de declaração de variáveis
			table.string('title').notNullable()
			//Os exercícios abaixo são sobre nomenclatura de variáveis
			table.string('content').notNullable()
			//posição na classe
			table.integer('position').notNullable()
			table.enu('difficulty', ['EASY', 'MEDIUM', 'HARD']).notNullable()
			table
				.integer('class_id')
				.unsigned()
				.references('id')
				.inTable('classes')
				.onDelete('CASCADE')
				.onUpdate('CASCADE')
				.notNullable()
			table.timestamps()
		})
	}

	down() {
		this.drop('exercises')
	}
}

module.exports = ExercisesSchema
